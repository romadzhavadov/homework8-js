const paragraphs = document.querySelectorAll('p');

paragraphs.forEach((elem) => {
    elem.style.backgroundColor = '#ff0000'
})

const optionsList = document.getElementById("optionsList")


for (let i = 0; i < optionsList.childNodes.length; i++) {
    console.log(optionsList.childNodes[i]);
}

console.log(optionsList);
console.log(optionsList.parentNode);
console.log(optionsList.childNodes);


const testParagraph = document.querySelector('#testParagraph');

testParagraph.innerHTML = 'This is a paragraph'

console.log(testParagraph);

const elemMainheader = document.querySelectorAll('.main-header li');

elemMainheader.forEach((elem) => {
    elem.classList.add('nav-item');
})

console.log(elemMainheader);


const elemSectionTitle = document.querySelectorAll('.section-title');

elemSectionTitle.forEach((elem) => {
    elem.classList.remove('section-title');
})

console.log(elemSectionTitle);